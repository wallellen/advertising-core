package com.xtzn.core.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xtzn.core.mapper.IRetainedListenningMapper;
import com.xtzn.core.service.IRetainedListenningService;

/**
 * 功能描述: <br>
 * **
 * 
 * @author zyw
 * @version [版本号, 2018年4月4日]
 */
@Service
public class RetainedListenningServiceImpl implements IRetainedListenningService {

	@Autowired
	private IRetainedListenningMapper retainedListenningDao;
	/**
	 * 功能描述: <br>
	 * 插入监控新数据
	 *
	 * @author zyw
	 * @version [版本号, 2018年4月4日]
	 */
	@Override
	public int insertRetainedListenning(Integer retainedId) {
		// TODO Auto-generated method stub
		return retainedListenningDao.insertRetainedListenning(retainedId);
	}

	/**
	 * 功能描述: <br>
	 * 处理过期数据
	 *
	 * @author zyw
	 * @version [版本号, 2018年4月4日]
	 */
	@Override
	public int updateOverdueRetained() {
		// TODO Auto-generated method stub
		return retainedListenningDao.updateOverdueRetained();
	}

	/**
	 * 功能描述: <br>
	 * 查询过期数据
	 *
	 * @author zyw
	 * @version [版本号, 2018年4月4日]
	 */
	@Override
	public List<Map<String, Object>> selectOverdueRetained() {
		// TODO Auto-generated method stub
		return retainedListenningDao.selectOverdueRetained();
	}

}
