package com.xtzn.core.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xtzn.core.mapper.ITaskListMapper;
import com.xtzn.core.service.ITaskListService;
import com.xtzn.core.vo.TaskResponse;
@Service
public class TaskListServiceImpl implements ITaskListService {

	@Autowired
	private ITaskListMapper taskListDao; 
	@Override
	public List<Map<String, Object>> selectForResetTask() {
		// TODO Auto-generated method stub
		return taskListDao.selectForResetTask();
	}
	
	@Override
	public int updateTimelinedtl(List<Map<String,Object>> list) {
		// TODO Auto-generated method stub
		return taskListDao.updateTimelinedtl(list);
	}


	/**
	 * 功能描述: <br>
	 * 查询今日需要分派的任务id,timelinedtl
	 *
	 * @author zyw
	 * @version [版本号, 2018年3月20日]
	 */
	@Override
	public List<Map<String, Object>> selectForDistributeTask() {
		// TODO Auto-generated method stub
		return taskListDao.selectForDistributeTask();
	}

	/**
	 * 功能描述: <br>
	 * 根据生成的留存类型查询所有的任务id
	 * 
	 * @param generateRetainedType 生成留存的类型 
	 * @author zyw
	 * @version [版本号, 2018年3月20日]
	 */
	@Override
	public List<Map<String, Object>> selectByGenerateRetainedType(Byte type) {
		// TODO Auto-generated method stub
		return taskListDao.selectByGenerateRetainedType(type);
	}

	/**
	 * 功能描述: <br>
	 * 查询抢量模式可以执行的任务
	 *
	 * @author zyw
	 * @version [版本号, 2018年3月30日]
	 */
	@Override
	public TaskResponse selectRobTaskData(Integer userId) {
		
		return taskListDao.selectRobTaskData(userId);
	}

	/**
	 * 功能描述: <br>
	 * 查询普通模式可以执行的任务
	 *
	 * @author zyw
	 * @version [版本号, 2018年3月30日]
	 */
	@Override
	public List<TaskResponse> selectNormalTaskData(Byte type,Integer userId) {
		
		return taskListDao.selectNormalTaskData(type,userId);
	}

	/**
	 * 功能描述: <br>
	 * 派发任务后更新任务待执行数
	 *
	 * @author zyw
	 * @version [版本号, 2018年4月2日]
	 */
	@Override
	public int updateDistributeTaskWatingForNum(Integer id) {
		
		return taskListDao.updateDistributeTaskWatingForNum(id);
	}

	/**
	 * 功能描述: <br>
	 * 批量更新待执行任务数
	 *
	 * @author zyw
	 * @version [版本号, 2018年4月2日]
	 */
	@Override
	public int updateBatchWaitingForNumAdd(List<Integer> list) {
		// TODO Auto-generated method stub
		return taskListDao.updateBatchWaitingForNumAdd(list);
	}

	/**
	 * 功能描述: <br>
	 * **
	 *
	 * @author zyw
	 * @version [版本号, 2018年4月21日]
	 */
	@Override
	public int updateBatchDistributeTaskWatingForNum(List<Integer> list) {
		// TODO Auto-generated method stub
		return taskListDao.updateBatchDistributeTaskWatingForNum(list);
	}

	/**
	 * 功能描述: <br>
	 * **
	 *
	 * @author zyw
	 * @version [版本号, 2018年4月26日]
	 */
	@Override
	public int updateLinkStateNormal() {
		// TODO Auto-generated method stub
		return taskListDao.updateLinkStateNormal();
	}
	

}
