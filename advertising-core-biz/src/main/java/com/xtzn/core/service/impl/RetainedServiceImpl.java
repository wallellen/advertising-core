
package com.xtzn.core.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xtzn.core.mapper.IRetainedListMapper;
import com.xtzn.core.service.IRetainedListService;
import com.xtzn.core.vo.RetainedResponse;

/**
 * 功能描述: <br>
 * 留存serviceImpl
 * 
 * @author zyw
 * @version [版本号, 2018年3月20日]
 */
@Service
public class RetainedServiceImpl implements IRetainedListService {

	@Autowired
	private IRetainedListMapper retainedListDao;
	/**
	 * 功能描述: <br>
	 * 清空留存
	 *
	 * @author zyw
	 * @version [版本号, 2018年3月20日]
	 */
	@Override
	public int updateEmptyRetained() {
		// TODO Auto-generated method stub
		return retainedListDao.updateEmptyRetained();
	}
	/**
	 * 功能描述: <br>
	 * 根据taskId查询该任务的留存 上传天数以及对应的任务数量
	 *
	 * @author zyw
	 * @version [版本号, 2018年3月22日]
	 */
	@Override
	public List<Map<String, Object>> selectGroupByTaskId(Integer taskId) {
		// TODO Auto-generated method stub
		return retainedListDao.selectGroupByTaskId(taskId);
	}
	/**
	 * 功能描述: <br>
	 * 更新普通任务留存状态
	 *
	 * @author zyw
	 * @version [版本号, 2018年3月22日]
	 */	
	@Override
	public int updateRetainedStatusForUsual(Integer taskId, Integer daysCnt, Integer taskCnt) {
		// TODO Auto-generated method stub
		return retainedListDao.updateRetainedStatusForUsual(taskId, daysCnt, taskCnt);
	}
	/**
	 * 功能描述: <br>
	 * 更新长时间任务留存状态
	 *
	 * @author zyw
	 * @version [版本号, 2018年3月22日]
	 */
	@Override
	public int updateRetainedStatusForSpecial(Integer taskId, Integer daysCnt, Integer taskCnt) {
		// TODO Auto-generated method stub
		return retainedListDao.updateRetainedStatusForSpecial(taskId, daysCnt, taskCnt);
	}
	/**
	 * 功能描述: <br>
	 * 修改关卡留存type
	 *
	 * @author zyw
	 * @version [版本号, 2018年3月23日]
	 */
	@Override
	public int updateLevelType(Integer taskId, Byte levelType, Integer limit) {
		// TODO Auto-generated method stub
		return retainedListDao.updateLevelType(taskId, levelType, limit);
	}
	/**
	 * 功能描述: <br>
	 * 更新关卡留存状态
	 *
	 * @author zyw
	 * @version [版本号, 2018年3月23日]
	 */
	@Override
	public int updateLevelRetained(Integer taskId, Integer daysCnt, Byte levelType, Integer taskCnt) {
		// TODO Auto-generated method stub
		return retainedListDao.updateLevelRetained(taskId, daysCnt, levelType, taskCnt);
	}
	/**
	 * 功能描述: <br>
	 * 修改次日关卡留存type
	 *
	 * @author zyw
	 * @version [版本号, 2018年3月23日]
	 */
	@Override
	public int updateMorrowLevelType(Integer taskId, Integer daysCnt) {
		// TODO Auto-generated method stub
		return retainedListDao.updateMorrowLevelType(taskId, daysCnt);
	}
	/**
	 * 功能描述: <br>
	 * 更新每日关卡留存特定次数的状态
	 *
	 * @author zyw
	 * @version [版本号, 2018年3月23日]
	 */
	@Override
	public int updateEverydayLevelRetained(Integer taskId, Integer daysCnt) {
		// TODO Auto-generated method stub
		return retainedListDao.updateEverydayLevelRetained(taskId, daysCnt);
	}
	/**
	 * 功能描述: <br>
	 * 插入新数据
	 *
	 * @author zyw
	 * @version [版本号, 2018年4月2日]
	 */
	@Override
	public int insertRetained(String subId) {
		// TODO Auto-generated method stub
		return retainedListDao.insertRetained(subId);
	}
	/**
	 * 功能描述: <br>
	 * 留存信息上传数据库
	 *
	 * @author zyw
	 * @version [版本号, 2018年4月3日]
	 */
	@Override
	public int updateRetained(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return retainedListDao.updateRetained(map);
	}
	/**
	 * 功能描述: <br>
	 * 派发留存任务获取可做留存,从昨日往前依次获取
	 * 
	 * @author zyw
	 * @version [版本号, 2018年4月3日]
	 */
	@Override
	public RetainedResponse selectRetainedData(Integer userId) {
		// TODO Auto-generated method stub
		return retainedListDao.selectRetainedData(userId);
	}
	/**
	 * 功能描述: <br>
	 * 留存二次登陆数据
	 *
	 * @author zyw
	 * @version [版本号, 2018年4月3日]
	 */
	@Override
	public List<RetainedResponse> selectRetainedSecondLoginData(Integer userId) {
		// TODO Auto-generated method stub
		return retainedListDao.selectRetainedSecondLoginData(userId);
	}
	/**
	 * 功能描述: <br>
	 * 将留存过期数据没有返回值的数据还原
	 *
	 * @author zyw
	 * @version [版本号, 2018年4月4日]
	 */
	@Override
	public int updateBatchOverdueRetained(List<Map<String, Object>> list) {
		// TODO Auto-generated method stub
		return retainedListDao.updateBatchOverdueRetained(list);
	}
	/**
	 * 功能描述: <br>
	 * **
	 *
	 * @author zyw
	 * @version [版本号, 2018年4月26日]
	 */
	@Override
	public int updateDistributeRetained(Integer id) {
		// TODO Auto-generated method stub
		return retainedListDao.updateDistributeRetained(id);
	}

}
