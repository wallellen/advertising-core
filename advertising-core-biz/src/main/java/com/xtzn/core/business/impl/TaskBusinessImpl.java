package com.xtzn.core.business.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.xtzn.core.business.ITaskBusiness;
import com.xtzn.core.commom.TypeCommon;
import com.xtzn.core.enums.ResponseEnum;
import com.xtzn.core.service.IIphoneRuleService;
import com.xtzn.core.service.IRetainedListService;
import com.xtzn.core.service.IRetainedListenningService;
import com.xtzn.core.service.ISubService;
import com.xtzn.core.service.ITaskListService;
import com.xtzn.core.service.ITaskListenningService;
import com.xtzn.core.utils.CurrentTimeUtil;
import com.xtzn.core.vo.CSResponse;
import com.xtzn.core.vo.RetainedResponse;
import com.xtzn.core.vo.TaskResponse;

/**
 * 功能描述: <br>
 * 获得任务详细数据
 * 
 * @author zyw
 * @version [版本号, 2018年3月27日]
 */
@Component("taskBusiness")
public class TaskBusinessImpl implements ITaskBusiness {

	@Autowired
	private ITaskListService taskListService;
	@Autowired
	private ITaskListenningService taskListenningService;
	@Autowired
	private ISubService subService;
	@Autowired
	private IRetainedListService retainedService;
	@Autowired
	private IIphoneRuleService iphoneRuleService;
	@Autowired
	private IRetainedListenningService retainedListenningService;
	/**
	 * 功能描述: <br>
	 * 提供lua任务数据接口
	 *
	 * @author zyw
	 * @version [版本号, 2018年3月27日]
	 */
	@Override
	public CSResponse requestTaskData(String phoneCode,Byte type,Integer userId) {
		//查看该手机是否有在监控中的任务
		Map<String,Object> listener = taskListenningService.selectTaskByPhoneCode(phoneCode);
		if(listener!=null){
			taskListenningService.updateTaskByPhoneCode(phoneCode);
		}
		// 查询数据获得可以做的任务,先获取"MODEL_TYPE"为抢量模式的任务,没有再获取普通模式		
		TaskResponse task = taskListService.selectRobTaskData(userId);
		String subId;
		if(task!=null){
			Integer id = task.getId();
			String landingPageLink = task.getLandingPageLink();
			subId = subService.getSubId();
			landingPageLink = landingPageLink.replace("[[subId]]", subId);
			task.setLandingPageLink(landingPageLink);
			task.setSubId(subId);;
			task.setCurrentTime(CurrentTimeUtil.currentTime());
			List<Map<String,Object>> list = iphoneRuleService.selectUrl(userId);
			List<Map<String,Object>> addressList = new ArrayList<Map<String,Object>>();			
			for(int i = 0;i < list.size();i++){
				Map<String,Object> map  = new HashMap<String,Object>();
				map.put((String)list.get(i).get("name"), list.get(i).get("url"));
				addressList.add(map);
			}
			task.setRuleAddress(addressList);
			taskListService.updateDistributeTaskWatingForNum(id);
			retainedService.insertRetained(subId);
			taskListenningService.insertTaskListenning(id, phoneCode);
			return new CSResponse(ResponseEnum.SUCCESS.getCode(),ResponseEnum.SUCCESS.getDesc(),(Serializable)task);
		}
		//获取普通模式的任务
		else{
			List<TaskResponse> taskList = taskListService.selectNormalTaskData(type,userId);
			if(!taskList.isEmpty()){
				//随机获取一个任务
				task = taskList.get((int)(Math.random()*taskList.size()));
				Integer id = task.getId();
				String landingPageLink = task.getLandingPageLink();
				subId = subService.getSubId();
				landingPageLink = landingPageLink.replace("[[subId]]", subId);
				task.setLandingPageLink(landingPageLink);
				task.setSubId(subId);;
				task.setCurrentTime(CurrentTimeUtil.currentTime());
				List<Map<String,Object>> list = iphoneRuleService.selectUrl(userId);
				List<Map<String,Object>> addressList = new ArrayList<Map<String,Object>>();			
				for(int i = 0;i < list.size();i++){
					Map<String,Object> map  = new HashMap<String,Object>();
					map.put((String)list.get(i).get("name"), list.get(i).get("url"));
					addressList.add(map);
				}
				task.setRuleAddress(addressList);
				taskListService.updateDistributeTaskWatingForNum(id);
				retainedService.insertRetained(subId);
				taskListenningService.insertTaskListenning(id, phoneCode);
				return new CSResponse(ResponseEnum.SUCCESS.getCode(),ResponseEnum.SUCCESS.getDesc(),(Serializable)task);
			}
		}
		//获取留存任务
		RetainedResponse map = retainedService.selectRetainedData(userId);
		//对留存的一次登陆进行监控
		if(null!=map){
			List<Map<String,Object>> list = iphoneRuleService.selectUrl(userId);
			List<Map<String,Object>> addressList = new ArrayList<Map<String,Object>>();			
			for(int i = 0;i < list.size();i++){
				Map<String,Object> ruleAddress  = new HashMap<String,Object>();
				ruleAddress.put((String)list.get(i).get("name"), list.get(i).get("url"));
				addressList.add(ruleAddress);
			}
			map.setRuleAddress(addressList);;
			Integer retainedId = map.getId();
			retainedService.updateDistributeRetained(retainedId);
			map.setDistributeType(TypeCommon.normalType);	
			map.setCurrentTime(CurrentTimeUtil.currentTime());
			retainedListenningService.insertRetainedListenning(retainedId);
			return new CSResponse(ResponseEnum.SUCCESS_留存.getCode(),ResponseEnum.SUCCESS_留存.getDesc(),(Serializable)map);
		}
		List<RetainedResponse> retainedlist = retainedService.selectRetainedSecondLoginData(userId);
		//不对留存的二次及多次登陆进行监控
		if(!retainedlist.isEmpty()){
			map = retainedlist.get((int)(Math.random()*retainedlist.size()));
			List<Map<String,Object>> list = iphoneRuleService.selectUrl(userId);
			List<Map<String,Object>> addressList = new ArrayList<Map<String,Object>>();			
			for(int i = 0;i < list.size();i++){
				Map<String,Object> ruleAddress  = new HashMap<String,Object>();
				ruleAddress.put((String)list.get(i).get("name"), list.get(i).get("url"));
				addressList.add(ruleAddress);
			}
			map.setRuleAddress(addressList);;
			map.setDistributeType(TypeCommon.normalType);	
			map.setCurrentTime(CurrentTimeUtil.currentTime());
			return new CSResponse(ResponseEnum.SUCCESS_留存.getCode(),ResponseEnum.SUCCESS_留存.getDesc(),(Serializable)map);
		}
		return new CSResponse(ResponseEnum.RETAINED_EEROR.getCode(),ResponseEnum.RETAINED_EEROR.getDesc());
		
	}
	

}
