package com.xtzn.core.business.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.xtzn.core.business.IRetainedBusiness;
import com.xtzn.core.commom.TypeCommon;
import com.xtzn.core.enums.ResponseEnum;
import com.xtzn.core.service.IIphoneRuleService;
import com.xtzn.core.service.IRetainedListService;
import com.xtzn.core.service.IRetainedListenningService;
import com.xtzn.core.utils.CurrentTimeUtil;
import com.xtzn.core.vo.CSResponse;
import com.xtzn.core.vo.RetainedResponse;

/**
 * 功能描述: <br>
 * 留存业务
 * 
 * @author zyw
 * @version [版本号, 2018年4月2日]
 */
@Component("retainedBusiness")
public class RetainedBusinessImpl implements IRetainedBusiness {

	@Autowired
	private IRetainedListService retainedService;
	@Autowired
	private IRetainedListenningService retainedListenningService;
	@Autowired
	private IIphoneRuleService iphoneRuleService;
	/**
	 * 功能描述: <br>
	 * 获取留存数据
	 *
	 * @author zyw
	 * @version [版本号, 2018年4月2日]
	 */
	@Override
	public CSResponse requestRetainedData(Integer userId) {
		RetainedResponse map = retainedService.selectRetainedData(userId);
		//对留存的一次登陆进行监控
		if(null!=map){
			List<Map<String,Object>> list = iphoneRuleService.selectUrl(userId);
			List<Map<String,Object>> addressList = new ArrayList<Map<String,Object>>();			
			for(int i = 0;i < list.size();i++){
				Map<String,Object> ruleAddress  = new HashMap<String,Object>();
				ruleAddress.put((String)list.get(i).get("name"), list.get(i).get("url"));
				addressList.add(ruleAddress);
			}
			map.setRuleAddress(addressList);;
			Integer retainedId = map.getId();
			retainedService.updateDistributeRetained(retainedId);
			map.setDistributeType(TypeCommon.normalType);	
			map.setCurrentTime(CurrentTimeUtil.currentTime());
			retainedListenningService.insertRetainedListenning(retainedId);
			return new CSResponse(ResponseEnum.SUCCESS_留存.getCode(),ResponseEnum.SUCCESS_留存.getDesc(),(Serializable)map);
		}
		List<RetainedResponse> retainedlist = retainedService.selectRetainedSecondLoginData(userId);
		//不对留存的二次及多次登陆进行监控
		if(!retainedlist.isEmpty()){
			map = retainedlist.get((int)(Math.random()*retainedlist.size()));
			List<Map<String,Object>> list = iphoneRuleService.selectUrl(userId);
			List<Map<String,Object>> addressList = new ArrayList<Map<String,Object>>();			
			for(int i = 0;i < list.size();i++){
				Map<String,Object> ruleAddress  = new HashMap<String,Object>();
				ruleAddress.put((String)list.get(i).get("name"), list.get(i).get("url"));
				addressList.add(ruleAddress);
			}
			map.setRuleAddress(addressList);;
			map.setDistributeType(TypeCommon.normalType);	
			map.setCurrentTime(CurrentTimeUtil.currentTime());
			return new CSResponse(ResponseEnum.SUCCESS_留存.getCode(),ResponseEnum.SUCCESS_留存.getDesc(),(Serializable)map);
		}
		return new CSResponse(ResponseEnum.RETAINED_EEROR.getCode(),ResponseEnum.RETAINED_EEROR.getDesc());
	}
	

}
