package com.xtzn.core.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

/**
 * 功能描述: <br>
 * **
 * 
 * @author zyw
 * @version [版本号, 2018年4月3日]
 */
public interface IRetainedListenningMapper {

	/**
	 * 
	 * 功能描述: <br>
	 * 插入监控新数据
	 * 
	 * @param retainedId 留存id
	 * @param distributeType 派发类型
	 * @author zyw
	 * @version [版本号, 2018年4月3日]
	 */
	public int insertRetainedListenning(@Param("retainedId")Integer retainedId);
	/**
	 * 	
	 * 功能描述: <br>
	 * 处理过期数据
	 * 
	 * @author zyw
	 * @version [版本号, 2018年4月3日]
	 */
	public int updateOverdueRetained();
	/**
	 * 
	 * 功能描述: <br>
	 * 查询过期数据
	 *  
	 * @author zyw
	 * @version [版本号, 2018年4月3日]
	 */
	public List<Map<String,Object>> selectOverdueRetained();
	
}
