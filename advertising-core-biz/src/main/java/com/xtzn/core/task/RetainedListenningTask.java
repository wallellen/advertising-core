package com.xtzn.core.task;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.dangdang.ddframe.job.api.JobExecutionMultipleShardingContext;
import com.dangdang.ddframe.job.plugin.job.type.simple.AbstractSimpleElasticJob;
import com.xtzn.core.service.IRetainedListService;
import com.xtzn.core.service.IRetainedListenningService;

/**
 * 功能描述: <br>
 * **
 * 
 * @author zyw
 * @version [版本号, 2018年4月2日]
 */
@Component
public class RetainedListenningTask extends AbstractSimpleElasticJob {

	@Autowired
	private IRetainedListenningService retainedListenningService;
	
	@Autowired
	private IRetainedListService retainedListService;
	/**
	 * 功能描述: <br>
	 * **
	 *
	 * @author zyw
	 * @version [版本号, 2018年4月2日]
	 */
	@Override
	public void process(JobExecutionMultipleShardingContext shardingContext) {
		// TODO Auto-generated method stub
		List<Map<String,Object>> list  = retainedListenningService.selectOverdueRetained();
		if(!list.isEmpty()){
			retainedListenningService.updateOverdueRetained();
			retainedListService.updateBatchOverdueRetained(list);
		}
	}

}
