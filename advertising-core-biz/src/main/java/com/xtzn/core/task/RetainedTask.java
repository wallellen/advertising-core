
package com.xtzn.core.task;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.dangdang.ddframe.job.api.JobExecutionMultipleShardingContext;
import com.dangdang.ddframe.job.plugin.job.type.simple.AbstractSimpleElasticJob;
import com.xtzn.core.service.IRetainedListService;
import com.xtzn.core.service.ITaskListService;

/**
 * 功能描述: <br>
 * 每日0点留存处理
 * 
 * @author zyw
 * @version [版本号, 2018年3月20日]
 */
@Component
public class RetainedTask extends AbstractSimpleElasticJob {

	@Autowired
	private IRetainedListService retainedListService;
	@Autowired
	private ITaskListService taskListService;
	/**
	 * 功能描述: <br>
	 * 每日0点清空昨日留存,重新生成今日留存
	 *
	 * @author zyw
	 * @version [版本号, 2018年3月20日]
	 */
	@Override
	public void process(JobExecutionMultipleShardingContext shardingContext) {
		// 清空昨日留存
		retainedListService.updateEmptyRetained();
		Integer taskId = null;
		String retainedPercent = null;
		Byte[] type = {0,1,2};
		for(int i = 0;i < 3;i++){
			//获得对应任务类型的taskId的list
			List<Map<String,Object>> list = taskListService.selectByGenerateRetainedType(type[i]);
			switch(i){
			//任务类型位普通类型
			case(0):
				//遍历taskId
				for(int j = 0;j < list.size();j++){
					//获得本次任务的taskId
					taskId = (Integer)list.get(j).get("id");
					//获取任务的留存百分比方案
					retainedPercent = (String)list.get(j).get("retainedPercent");
					//根据taskId获取留存上传时间距离今日多少天及对应的完成任务数量的list
					List<Map<String,Object>> countList = retainedListService.selectGroupByTaskId(taskId);
					//获取百分比方案的具体值
					String[] rept = retainedPercent.split(";");
					//遍历留存上传时间距离今日多少天及对应的完成任务数量的list
					for(int k = 0;k < countList.size();k++){
						//获取天数差
						int daysCnt = (Integer)countList.get(k).get("daysCnt");
						//获取当天完成的任务数量
						Long idCnt = (Long)countList.get(k).get("idCnt");
						//获得今日对应的百分比
						double pert = Double.valueOf(rept[daysCnt]);
						//获取今日需要完成的对应的留存任务数
						int taskCnt = (int)Math.ceil(idCnt*pert/100);
						//更新任务
						retainedListService.updateRetainedStatusForUsual(taskId, daysCnt, taskCnt);					
					}
				}
		     	break;    
			//任务类型为长时间类型
			case(1):
				//遍历taskId
				for(int j = 0;j < list.size();j++){
					//获得本次任务的taskId
					taskId = (Integer)list.get(j).get("id");
					//获取任务的留存百分比方案
					retainedPercent = (String)list.get(j).get("retainedPercent");
					//根据taskId获取留存上传时间距离今日多少天及对应的完成任务数量的list
					List<Map<String,Object>> countList = retainedListService.selectGroupByTaskId(taskId);
					//获取百分比方案的具体值
					String[] rept = retainedPercent.split(";");
					//遍历留存上传时间距离今日多少天及对应的完成任务数量的list
					for(int k = 0;k < countList.size();k++){
						//获取天数差
						int daysCnt = (Integer)countList.get(k).get("daysCnt");
						//获取当天完成的任务数量
						Long idCnt = (Long)countList.get(k).get("idCnt");
						//获得今日对应的百分比
						double pert = Double.valueOf(rept[daysCnt]);
						//获取今日需要完成的对应的留存任务数
						int taskCnt = (int)Math.ceil(idCnt*pert/100);
						//更新任务
						retainedListService.updateRetainedStatusForSpecial(taskId, daysCnt, taskCnt);					
					}
				}
			    break;
			//任务类型为关卡类型	
			case(2):
				//遍历taskId
				for(int j = 0;j < list.size();j++){
					//获得本次任务的taskId
					taskId = (Integer)list.get(j).get("id");
					//获取任务的留存百分比方案
					retainedPercent = (String)list.get(j).get("retainedPercent");
					//获取关卡模式次日特定次数百分比
					double morrowLevelPercent = (Double)list.get(j).get("morrowLevelPercent");
					//获取关卡模式每日特定次数百分比
					double everydayLevelPercent = (Double)list.get(j).get("everydayLevelPercent");
					//根据taskId获取留存上传时间距离今日多少天及对应的完成任务数量的list
					List<Map<String,Object>> countList = retainedListService.selectGroupByTaskId(taskId);
					//获取百分比方案的具体值
					String[] rept = retainedPercent.split(";");
					//遍历留存上传时间距离今日多少天及对应的完成任务数量的list
					for(int k = 0;k < countList.size();k++){
						//获取天数差
						int daysCnt = (Integer)countList.get(k).get("daysCnt");
						//获取当天完成的任务数量
						Long idCnt = (Long)countList.get(k).get("idCnt");
						//获得今日对应的百分比
						double pert = Double.valueOf(rept[daysCnt]);
						//获取今日需要完成的对应的留存任务数
						int taskCnt = (int)Math.ceil(idCnt*pert/100);
						//次日关卡留存
						if(daysCnt == 1){
							//获得次日特定次数任务数
							int morrowCnt = (int)Math.ceil(idCnt*morrowLevelPercent);
							//获取每日特定次数任务数
							int everydayCnt = (int)Math.ceil(idCnt*everydayLevelPercent);
							//获取普通次数任务数
							int usualCnt = taskCnt - morrowCnt - everydayCnt;
							//修改关卡留存的type类型
							retainedListService.updateLevelType(taskId, type[0], everydayCnt);
							retainedListService.updateLevelType(taskId, type[1], morrowCnt);
							retainedListService.updateLevelRetained(taskId, daysCnt, type[0], everydayCnt);
							retainedListService.updateLevelRetained(taskId, daysCnt, type[1], morrowCnt);
							retainedListService.updateLevelRetained(taskId, daysCnt, type[2], usualCnt);
						}
						if(daysCnt == 2){
							//3日先将次日特殊type修改为正常
							retainedListService.updateMorrowLevelType(taskId, daysCnt);
							int everydayCnt = retainedListService.updateEverydayLevelRetained(taskId, daysCnt);
							retainedListService.updateLevelRetained(taskId, daysCnt, type[2], taskCnt-everydayCnt);
						}
						else{
							int everydayCnt = retainedListService.updateEverydayLevelRetained(taskId, daysCnt);
							retainedListService.updateLevelRetained(taskId, daysCnt, type[2], taskCnt-everydayCnt);
						}											
					}
				}
			}
		}
		
	}

}
