/**
 * 功能描述: <br>
 * **
 *
 * @author zyw
 * @version [版本号, 2018年3月20日]
 */
package com.xtzn.core.task;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.dangdang.ddframe.job.api.JobExecutionMultipleShardingContext;
import com.dangdang.ddframe.job.plugin.job.type.simple.AbstractSimpleElasticJob;
import com.xtzn.core.service.ITaskListService;

/**
 * 功能描述: <br>
 * 派发任务
 * 
 * @author zyw
 * @version [版本号, 2018年3月20日]
 */
@Component
public class DistributeTask extends AbstractSimpleElasticJob {

	
	@Autowired
	private ITaskListService taskListService;
	/**
	 * 功能描述: <br>
	 * 每个30秒执行一次
	 * 
	 * @author zyw
	 * @version [版本号, 2018年3月20日]
	 */
	@Override
	public void process(JobExecutionMultipleShardingContext shardingContext) {
		// 获得任务list
		List<Map<String,Object>> list = taskListService.selectForDistributeTask();
		if(list.isEmpty()){
			return;
		}
		List<Integer> taskList = new ArrayList<Integer>();
		String timelinedtl = null;
		Integer taskId = null;
		Date date = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		//获得当前小时及分钟
		int hour = calendar.get(Calendar.HOUR_OF_DAY);
		int minute = calendar.get(Calendar.MINUTE);
		if(list.size()>0){
			for(int i = 0;i < list.size();i++){
				//获得该任务的时间线具体详情
				timelinedtl = (String)list.get(i).get("TIMELINEDTL");
				//获得任务Id
				taskId = (Integer)list.get(i).get("ID");
				String[] hours = timelinedtl.split(";");
				//如果该小时内没有任务，则hours[hour]=""
				if(!hours[hour].equals("")){
					String[] minutes = hours[hour].split(",");
					for(int j = 0;j < minutes.length;j++){
						//当前分钟时间域设置的分钟时间一致，派发任务
						if(minute == Integer.parseInt(minutes[j])){
							taskList.add(taskId);
						}
					}
				}
			}
		}
		if(!taskList.isEmpty()){
			taskListService.updateBatchWaitingForNumAdd(taskList);
		}
		
		
	}

}
